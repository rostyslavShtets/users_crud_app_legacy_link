#!/usr/bin/env bash

CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o main
docker build -t myapp_test_legacy_link .
# docker build -t myapp_test_legacy_link -f Dockerfile.scratch .
# docker run -it --name myapp --link some-sql:db -p 8002:8080 myapp