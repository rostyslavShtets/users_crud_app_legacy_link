package main

import (
	"users_crud_app_link/models"
	"github.com/go-chi/chi"
	"net/http"
	"fmt"
	"log"
)
const port = ":8080"

func main() {
	models.Hello()
	models.InitDB("root:root@tcp(db:3306)/people")
	// models.InitDB("root:root@tcp(:3307)/people")
	
	r := chi.NewRouter()
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte(".root"))
	})
	r.Get("/people", models.GetPeople)
	r.Get("/people/{personID}", models.GetPerson)
	r.Post("/people", models.CreatePerson)
	r.Put("/people/{personID}", models.UpdatePerson)
	r.Delete("/people/{personID}", models.DeletePerson)

	fmt.Printf("Serv on port %v....\n", port)
	log.Fatal(http.ListenAndServe(port, r))
}

