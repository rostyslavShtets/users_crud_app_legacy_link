#FROM golang:latest
# RUN apt-get install -y ca-certificates
# RUN go get -u github.com/gorilla/mux
# RUN mkdir /app
# ADD . /app/
# WORKDIR /app
# RUN go build -o main .
# EXPOSE 8080
# CMD ["/app/main"]

# docker run -it --name some-sql -e MYSQL_ROOT_PASSWORD=root -d mysql

FROM bitnami/minideb
ADD main /
EXPOSE 80
CMD ["/main"]