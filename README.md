My app with docker. 

THE MAIN TARGET - To connect containers with help of legacy link (before docker-composer).

CONTENTS OF DOCKERFILE:
    "FROM bitnami/minideb
    ADD main /
    EXPOSE 80
    CMD ["/main"]"

CONNECTION STRING:
    models.InitDB("root:root@tcp(db:3306)/people")

0. BUILD IMAGE 
    CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -o main
    docker build -t myapp_test_legacy_link .

1. UP DATABASE IN CONTAINER FROM MYSQL IMAGE(DOCKER HUB)
    docker run -it --name sql_link -e MYSQL_ROOT_PASSWORD=root -d mysql
2. LAUNCH MY APP IN CONTAINER AND LINK TO MYSQL
    docker run -d --name my-test-link_app -p 8003:8080 --link sql_link:db myapp_test_legacy_link
3. TO FILL THE MYSQL DATABASE WITH SOME DATA
    docker exec -it 847 mysql --user=root --password=root

CREATING BASE
    CREATE DATABASE people;

CREATING TABLE
    CREATE TABLE people (userID int NOT NULL AUTO_INCREMENT PRIMARY KEY, FirstName varchar(40), LastName varchar(40), Age int(3));

INSERT DATA TO TABLE
    INSERT INTO people(FirstName, LastName, Age) VALUES ('John', 'Docker', 135)
